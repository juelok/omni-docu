# Фильтр

## Клиенты

Как происходит фильтрация

**Особенности сайджеста:**

* выводятся и клиенты, и сотрудники
* могут быть клиенты и сотрудники, по выбору которых будет заглушка с котиком
* имя клиента, которого не было вообще в системе, не должно появляться в списке вариантов

## Каналы

Отображаются все (активные) каналы текущего КЦ и каналы других КЦ, по которым были диалоги в текущем КЦ за последние 6 месяцев

## Категории

Как происходит фильтрация

**Особенности сайджеста:**

* выводятся активные и неактивные категории КЦ пользователя
